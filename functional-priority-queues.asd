(defpackage functional-priority-queues-system (:use :cl :asdf))
(in-package functional-priority-queues-system)

(defsystem :functional-priority-queues
  :author "Phil! Gold <phil_g@pobox.com>"
  :components ((:file "packages")
               (:file "generics" :depends-on ("packages"))
               (:file "skew-binomial-queues" :depends-on ("generics"))
               (:file "global-root" :depends-on ("generics"))
               (:file "bootstrapped" :depends-on ("global-root"))
               (:file "pairing-heap" :depends-on ("generics")))
  :in-order-to ((test-op (test-op :functional-priority-queues/tests))))

(defsystem :functional-priority-queues/tests
  :depends-on (:functional-priority-queues :fiveam)
  :components ((:file "fpq-tests"))
  :perform (test-op (o c) (uiop:symbol-call :fiveam :run! :functional-priority-queues)))
