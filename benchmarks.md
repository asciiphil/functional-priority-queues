Running the [benchmarks from damn-fast-priority-queue][damn-fast-benchmark]
with this library gave the following results:

  [damn-fast-benchmark]: https://github.com/phoe/damn-fast-priority-queue/tree/main/priority-queue-benchmark

|                       |    inc.   |    dec.   |    rand.   |
|-----------------------|----------:|----------:|-----------:|
| damn-fast...          |   0.671   |   0.831   |    0.963   |
| pileup                |   2.231   |   2.603   |    2.459   |
| pettomato             |   4.879   |   6.923   |    8.203   |
| minheap               |   6.311   |   9.243   |    8.387   |
| priority-queue        |   7.579   |  12.055   |    8.695   |
| heap                  |  10.259   |  14.567   |   10.843   |
| queues                |   9.315   |   9.231   |   16.227   |
| **fpq pairing heap**  | **3.623** | **1.123** | **16.759** |
| **fpq skew binomial** | **8.023** | **5.267** |            |
| cl-heap               |  12.439   |  14.495   |   34.359   |

The default benchmark settings were used (409600 items, 10 repetitions,
capacity information supplied to libraries that supported it).  The table
is sorted by performance on the shuffled input.

The skew binomial function priority queue runs out of heap space on the
randomized input.
