(in-package :functional-priority-queues)

(defgeneric empty? (queue)
  (:documentation
   "Returns T if QUEUE is empty, NIL otherwise."))

(defgeneric insert (queue item &optional priority)
  (:documentation
   "Returns a new queue with ITEM added to it.  If PRIORITY is not
   provided, the item's priority will be derived from the queue's key
   function, if it exists, or the item itself."))

(defgeneric meld (queue1 queue2)
  (:documentation
   "Returns a new queue comprising all the members of QUEUE1 and
   QUEUE2."))

(defgeneric find-root (queue)
  (:documentation
   "Returns two values: the root element of the queue and its priority."))

(defgeneric delete-root (queue)
  (:documentation
   "Returns three values: a new queue with the previous root element
   removed, the removed root element, and the removed element's
   priority."))

(defun empty-queue (&key (pred #'<=) (key #'identity) (type :pairing-heap))
  "Returns an empty queue.

  PRED should be a function of two arguments that returns true if the
  first argument should come before the second argument or if both
  arguments are equal.  The default value is #'<=, which will construct a
  min heap over numeric values.

  KEY should be a function of one value that, when passed an object to be
  added to the heap, returns a value that can be compared by PRED.  The
  default is to use the inserted objects themselves in calls to PRED.

  TYPE gives the type of queue to create.  It should be one of the
  following:
   * :pairing-heap (default)
   * :skew-binomial
   * :skew-binomial-global-root
   * :skew-binomial-bootstrapped

  See the README file for descriptions of the types of queues."
  (ecase type
    (:pairing-heap (make-empty-pairing-heap :pred-fn pred :key-fn key))
    (:skew-binomial (make-sb-forest :pred-fn pred :key-fn key))
    (:skew-binomial-global-root
     (make-empty-gr-queue
      :pred-fn pred :key-fn key
      :empty-queue (empty-queue :type :skew-binomial :pred pred :key key)))
    (:skew-binomial-bootstrapped
     (make-empty-bs-queue
      :pred-fn pred :key-fn key
      :empty-queue (empty-queue :type :skew-binomial :pred pred :key key)))))

(defstruct (functional-priority-queue (:conc-name fpq-))
  (key-fn #'identity
   :type (function (*))
   :read-only t)
  (pred-fn #'<=
   :type (function (* *))
   :read-only t))


;;;;;;;;;;;;;;
;;; Type: node
;;;
;;; A "node" is a combination of the value held by the queue and its
;;; priority.

(deftype node (&optional element-type priority-type)
  `(cons ,element-type ,priority-type))

(defun make-node (queue value priority use-priority-p)
  (cons (if use-priority-p
            priority
            (funcall (fpq-key-fn queue) value))
        value))

(defun node-priority (node)
  (car node))

(defun node-value (node)
  (cdr node))

(declaim (inline node-leq))
(defun node-leq (queue node1 node2)
  (declare (type functional-priority-queue queue)
           (type node node1 node2)
           (optimize (speed 3)))
  (funcall (the (function (* *)) (slot-value queue 'pred-fn))
           (node-priority node1)
           (node-priority node2)))
