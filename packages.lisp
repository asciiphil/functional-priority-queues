(defpackage :functional-priority-queues
  (:nicknames :fpq)
  (:use :common-lisp)
  (:export
   :empty?
   :find-root
   :insert
   :meld
   :delete-root
   :empty-queue))
