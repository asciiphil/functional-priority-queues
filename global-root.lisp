(in-package :functional-priority-queues)


(defstruct (gr-queue (:include functional-priority-queue))
  "Class to wrap another queue type with O(1) find-root operations."
  (empty-queue nil
   :type functional-priority-queue
   :read-only t))

(defstruct (empty-gr-queue (:include gr-queue)))

(defstruct (full-gr-queue (:include gr-queue))
  (node nil
   :type node
   :read-only t)
  (queue nil
   :type functional-priority-queue
   :read-only t))

(defun update-empty-gr-queue (queue)
  "This might seem like it's pointless, but it exists to turn full global
  root queues into empty global root queues."
  (with-slots (key-fn pred-fn empty-queue) queue
    (make-empty-gr-queue :empty-queue empty-queue
                         :key-fn key-fn
                         :pred-fn pred-fn)))

(defun update-full-gr-queue (queue &key node (wrapped-queue nil queuep))
  (with-slots (key-fn pred-fn empty-queue) queue
    (make-full-gr-queue :node (or node (full-gr-queue-node queue))
                        :queue (if queuep
                                   wrapped-queue
                                   (slot-value queue 'queue))
                        :empty-queue empty-queue
                        :key-fn key-fn
                        :pred-fn pred-fn)))


(defmethod empty? ((queue empty-gr-queue)) t)
(defmethod empty? ((queue full-gr-queue)) nil)


(defmethod insert ((queue empty-gr-queue) item &optional (priority nil priorityp))
  (update-full-gr-queue queue
                        :node (make-node queue item priority priorityp)
                        :wrapped-queue (slot-value queue 'empty-queue)))

(defmethod insert ((queue full-gr-queue) item &optional (priority nil priorityp))
  (with-slots ((old-node node)) queue
    (let ((new-node (make-node queue item priority priorityp)))
      (if (node-leq queue new-node old-node)
          (update-full-gr-queue queue
                                :node new-node
                                :wrapped-queue (insert (full-gr-queue-queue queue)
                                                       (node-value old-node)
                                                       (node-priority old-node)))
          (update-full-gr-queue queue
                                :wrapped-queue (insert (full-gr-queue-queue queue)
                                                       (node-value new-node)
                                                       (node-priority new-node)))))))


(defmethod meld ((queue1 gr-queue) (queue2 empty-gr-queue)) queue1)
(defmethod meld ((queue1 empty-gr-queue) (queue2 gr-queue)) queue2)

(defmethod meld ((queue1 full-gr-queue) (queue2 full-gr-queue))
  (if (node-leq queue1 (full-gr-queue-node queue1) (full-gr-queue-node queue2))
      (update-full-gr-queue
       queue1
       :wrapped-queue (insert (meld (full-gr-queue-queue queue1)
                                    (full-gr-queue-queue queue2))
                              (node-value (full-gr-queue-node queue2))
                              (node-priority (full-gr-queue-node queue2))))
      (update-full-gr-queue
       queue2
       :wrapped-queue (insert (meld (full-gr-queue-queue queue1)
                                    (full-gr-queue-queue queue2))
                              (node-value (full-gr-queue-node queue1))
                              (node-priority (full-gr-queue-node queue1))))))


(defmethod find-root ((queue empty-gr-queue))
  (error "Cannot get the root of an empty queue."))

(defmethod find-root ((queue full-gr-queue))
  (values (node-value (full-gr-queue-node queue))
          (node-priority (full-gr-queue-node queue))))


(defmethod delete-root ((queue empty-gr-queue))
  (error "Cannot delete the root of an empty queue."))

(defmethod delete-root ((queue full-gr-queue))
  (if (empty? (full-gr-queue-queue queue))
      (values (update-empty-gr-queue queue)
              (node-value (full-gr-queue-node queue))
              (node-priority (full-gr-queue-node queue)))
      (multiple-value-bind (new-queue root-value root-priority)
          (delete-root (full-gr-queue-queue queue))
        (values (update-full-gr-queue
                 queue
                 :node (make-node queue root-value root-priority t)
                 :wrapped-queue new-queue)
                (node-value (full-gr-queue-node queue))
                (node-priority (full-gr-queue-node queue))))))
