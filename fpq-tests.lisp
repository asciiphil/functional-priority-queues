(defpackage :functional-priority-queues-tests
  (:use :common-lisp :fiveam))

(in-package :functional-priority-queues-tests)

(def-suite :functional-priority-queues)
(in-suite :functional-priority-queues)

(eval-when (:compile-toplevel :execute)
  (defparameter +queue-types+
    '(:pairing-heap :skew-binomial :skew-binomial-global-root :skew-binomial-bootstrapped)))

;;; All of the queue implementations should behave identically with
;;; respect to the API functions.  This macro will run the same tests on
;;; each implementation.
(defmacro def-fpq-test (base-name (queue &rest queue-opts) &body body)
  `(progn
     ,@(reduce (lambda (result queue-type)
                 (let ((test-name (intern (format nil "~A-~A" base-name queue-type))))
                   (cons `(test ,test-name
                            (let ((,queue (fpq:empty-queue :type ,queue-type ,@queue-opts)))
                              ,@body))
                         result)))
               +queue-types+
               :initial-value nil)))

(defun queue-to-list (queue)
  (labels ((list-r (remaining-queue result)
             (if (fpq:empty? remaining-queue)
                 (reverse result)
                 (multiple-value-bind (next-queue new-value)
                     (fpq:delete-root remaining-queue)
                   (list-r next-queue (cons new-value result))))))
    (list-r queue nil)))

(def-fpq-test empty (queue)
  (is-true (fpq:empty? queue)))

(def-fpq-test add-and-remove-number (empty-queue)
  (is-true (fpq:empty? empty-queue))
  (let ((full-queue (fpq:insert empty-queue 5)))
    (is-false (fpq:empty? full-queue))
    (is (= 5 (fpq:find-root full-queue)))
    (multiple-value-bind (emptied-queue value priority)
        (fpq:delete-root full-queue)
      (is-true (fpq:empty? emptied-queue))
      (is (= 5 value))
      (is (= 5 priority))
      ;; Verify immutability
      (is-false (fpq:empty? full-queue))
      (is-true (fpq:empty? empty-queue)))))

(def-fpq-test sort-numbers (empty-queue)
  (let* ((numbers '(62 7 13 68 99 59 19 87 11 42 24 28 15 52 34 33 75 11 82 82 67 60 24 53))
         (full-queue (reduce #'fpq:insert
                             numbers
                             :initial-value empty-queue))
         (sorted-numbers (queue-to-list full-queue)))
    (is (equal (sort (copy-list numbers) #'<) sorted-numbers))))

(def-fpq-test reverse-sort-numbers (empty-queue :pred #'>=)
  (let* ((numbers '(62 7 13 68 99 59 19 87 11 42 24 28 15 52 34 33 75 11 82 82 67 60 24 53))
         (full-queue (reduce #'fpq:insert
                             numbers
                             :initial-value empty-queue))
         (sorted-numbers (queue-to-list full-queue)))
    (is (equal (sort (copy-list numbers) #'>) sorted-numbers))))

(def-fpq-test string-length (empty-queue :key #'length)
  (let* ((words '("appraising"
                  "appreciates"
                  "apprise"
                  "apse"
                  "apt"
                  "aquae"))
         (full-queue (reduce #'fpq:insert words :initial-value empty-queue))
         (dequeued-words (queue-to-list full-queue))
         (sorted-words (sort (copy-list words) #'< :key #'length)))
    (is-true (endp (remove t (mapcar #'string= sorted-words dequeued-words)))
             "~A was not equal to ~A" sorted-words dequeued-words)))
