(in-package :functional-priority-queues)


(defstruct (empty-pairing-heap (:include functional-priority-queue)))

(defstruct (pairing-tree (:include functional-priority-queue))
  (node nil
   :type node
   :read-only t)
  (subtree nil
   :type list
   :read-only t))

(defun update-empty-pairing-heap (heap)
  "This might seem like it's pointless, but it exists to turn pairing
   trees into empty pairing heaps."
  (with-slots (key-fn pred-fn) heap
    (make-empty-pairing-heap
     :key-fn key-fn
     :pred-fn pred-fn)))

(defun update-pairing-tree (tree &key node (subtree nil subtreep))
  (with-slots (key-fn pred-fn) tree
    (make-pairing-tree
     :node (or node (pairing-tree-node tree))
     :subtree (if subtreep
                  subtree
                  (pairing-tree-subtree tree))
     :key-fn key-fn
     :pred-fn pred-fn)))


(defmethod empty? ((heap empty-pairing-heap)) t)
(defmethod empty? ((tree pairing-tree)) nil)


(defmethod insert ((heap empty-pairing-heap) item &optional (priority nil priorityp))
  (update-pairing-tree heap
                       :node (make-node heap item priority priorityp)
                       :subtree nil))

(defmethod insert ((tree pairing-tree) item &optional (priority nil priorityp))
  (let ((new-node (make-node tree item priority priorityp)))
    (if (node-leq tree new-node (pairing-tree-node tree))
        (update-pairing-tree tree
                             :node new-node
                             :subtree (list tree))
        (update-pairing-tree tree
                             :subtree (cons (update-pairing-tree tree
                                                                 :node new-node
                                                                 :subtree nil)
                                            (pairing-tree-subtree tree))))))


(defmethod meld (heap1 (heap2 empty-pairing-heap)) heap1)
(defmethod meld ((heap1 empty-pairing-heap) heap2) heap2)

(defmethod meld ((tree1 pairing-tree) (tree2 pairing-tree))
  (if (node-leq tree1 (pairing-tree-node tree1) (pairing-tree-node tree2))
      (update-pairing-tree tree1 :subtree (cons tree2 (pairing-tree-subtree tree1)))
      (update-pairing-tree tree2 :subtree (cons tree1 (pairing-tree-subtree tree2)))))


(defmethod find-root ((heap empty-pairing-heap))
  (error "Cannot get the root of an empty queue."))

(defmethod find-root ((tree pairing-tree))
  (values (node-value (pairing-tree-node tree))
          (node-priority (pairing-tree-node tree))))


(defmethod delete-root ((heap empty-pairing-heap))
  (error "Cannot delete the root of an empty queue."))

(defun pairing-tree-merge-pairs (trees &optional pairs)
  (declare (type list trees))
  (cond
    ((endp trees)
     (assert pairs)
     (reduce #'meld pairs))
    ((endp (cdr trees))
     (pairing-tree-merge-pairs nil (cons (car trees) pairs)))
    (t
     (destructuring-bind (tree1 tree2 &rest other-trees) trees
       (pairing-tree-merge-pairs other-trees (cons (meld tree1 tree2) pairs))))))

(defmethod delete-root ((tree pairing-tree))
  (if (endp (pairing-tree-subtree tree))
      (values (update-empty-pairing-heap tree)
              (node-value (pairing-tree-node tree))
              (node-priority (pairing-tree-node tree)))
      (values (pairing-tree-merge-pairs (pairing-tree-subtree tree))
              (node-value (pairing-tree-node tree))
              (node-priority (pairing-tree-node tree)))))
