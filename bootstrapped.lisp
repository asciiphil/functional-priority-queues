(in-package :functional-priority-queues)


;;; The bootstrapped queue classes are implemented as specializations of
;;; global root classes.  This is because the actual data structures are
;;; the same between the two, and some of the behaviors are, too.


(defstruct (empty-bs-queue (:include empty-gr-queue)))

(defstruct (full-bs-queue (:include full-gr-queue)))

;;; Needs to be a new function so the right object will be created.
(defun update-empty-bs-queue (queue)
  "This might seem like it's pointless, but it exists to turn full global
  root queues into empty global root queues."
  (with-slots (key-fn pred-fn empty-queue) queue
    (make-empty-bs-queue :empty-queue empty-queue
                         :key-fn key-fn
                         :pred-fn pred-fn)))

;;; Needs to be a new function so the right object will be created.
(defun update-full-bs-queue (queue &key node (wrapped-queue nil queuep))
  (with-slots (key-fn pred-fn empty-queue) queue
    (make-full-bs-queue :node (or node (full-bs-queue-node queue))
                        :queue (if queuep
                                   wrapped-queue
                                   (slot-value queue 'queue))
                        :empty-queue empty-queue
                        :key-fn key-fn
                        :pred-fn pred-fn)))


;;; `empty?' is fully inherited


;;; Needs to be a new function so the right object will be created.
(defmethod insert ((queue empty-bs-queue) item &optional (priority nil priorityp))
  (update-full-bs-queue queue
                        :node (make-node queue item priority priorityp)
                        :wrapped-queue (slot-value queue 'empty-queue)))

(defmethod insert ((queue full-bs-queue) item &optional (priority nil priorityp))
  (meld (update-full-bs-queue queue
                              :node (make-node queue item priority priorityp)
                              :wrapped-queue (slot-value queue 'empty-queue))
        queue))


(defmethod meld ((queue1 full-bs-queue) (queue2 full-bs-queue))
  (if (node-leq queue1 (full-bs-queue-node queue1) (full-bs-queue-node queue2))
      (update-full-bs-queue
       queue1
       :wrapped-queue (insert (full-bs-queue-queue queue1)
                              queue2
                              (node-priority (full-bs-queue-node queue2))))
      (update-full-bs-queue
       queue2
       :wrapped-queue (insert (full-bs-queue-queue queue2)
                              queue1
                              (node-priority (full-bs-queue-node queue1))))))


;;; `find-root' is fully inherited


(defmethod delete-root ((queue full-bs-queue))
  (if (empty? (full-bs-queue-queue queue))
      (values (update-empty-bs-queue queue)
              (node-value (full-bs-queue-node queue))
              (node-priority (full-bs-queue-node queue)))
      (multiple-value-bind (new-queue root-value root-priority)
          (delete-root (full-bs-queue-queue queue))
        (values (update-full-bs-queue
                 queue
                 :node (make-node queue
                                  (node-value (full-bs-queue-node root-value))
                                  root-priority t)
                 :wrapped-queue (meld (full-bs-queue-queue root-value)
                                      new-queue))
                (node-value (full-bs-queue-node queue))
                (node-priority (full-bs-queue-node queue))))))
