functional-priority-queues
==========================

Provides a pure functional implementation of priority queues for Common
Lisp.  The priority queues are immutable and the API functions have no
side effects.

The implementation is based on [The Pairing Heap: A New Form of
Self-Adjusting Heap][fredman-et-al] by Fredman, Sedgewick, Sleator, and
Tarjan and [Optimal Purely Functional Priority Queues][brodal-okasaki] by
Gerth Stølting Brodal and Chris Okasaki.

  [fredman-et-al]: https://www.cs.cmu.edu/~sleator/papers/pairing-heaps.pdf
  [brodal-okasaki]: https://www.cambridge.org/core/journals/journal-of-functional-programming/article/optimal-purely-functional-priority-queues/1A799209383FD6535F90715192863C13#.YcfRYYucZ70.link


Quick Introduction
------------------

Create an empty queue:

    (empty-queue)

`empty-queue` accepts `:pred` and `:key` parameters.  `:key` works like
you'd expect:

    ; Sort based on sequence length.
    (empty-queue :key #'length)

`:pred` should be a function of two parameters that returns true if the
first parameter should be sorted before the second parameter *or* if the
two parameters are equal.  The second qualifier about equality is distinct
from the predicate parameters to many standard functions, like `sort`.

    ; Make a max heap
    (empty-queue :pred #'>=)

Once you've created a queue, you can add items to it with `insert`, which
will return the new queue.

    (insert (empty-queue) 15)

`find-root` returns two values.  First, the item at the root of the queue;
second, the priority associated with the item.

    > (find-root (insert (empty-queue :key #'length) "foo"))
    "foo"
    3

`meld` merges two queues, returning a new queue.

`delete-root` removes the item at the root of the queue and returns three
values: first, the new queue without the removed item; second, the item
that was removed; third, the priority of the item that was removed.

    > (delete-root (insert (empty-queue :key #'length) "foobar"))
    #S(EMPTY-PAIRING-HEAP :KEY-FN #<FUNCTION LENGTH> :PRED-FN #<FUNCTION <=>)
    "foobar"
    6



Types of Queues Supported
-------------------------

The library supports several different priority queue implementations,
selected by the `:type` parameter to `empty-queue`.  The implementations
are as follows:

 * `:pairing-heap` - This is the default.  It's a fairly efficient queue
   implementation based on an algorithm of merging pairs of subtrees for
   `delete-root`.  It provides O(1) `insert`, `meld`, and `find-root` and
   amortized O(log *n*) `delete-root`.
 
 * `:skew-binomial` - Theoretically efficient queue implementation based
   on skew binomial trees.  It provides O(1) `insert` and O(log *n*)
   `meld`, `find-root`, and `delete-root` operations.
   
 * `:skew-binomial-global-root` - A modification of skew binomial trees to
   provide O(1) `find-root` operations at the cost of a slight amount of
   additional bookkeeping.  It's probably only worth using these (as
   opposed to the default skew binomial priority queues) if your code does
   a lot of calls to `find-root` without a corresponding large number of
   calls to `delete-root`.
   
 * `:skew-binomial-bootstrapped` - A further modification of skew binomial
   trees to also provide O(1) `meld` operations, leaving `delete-root` as
   the only O(log *n*) operation.  The tradeoff is more complex code and
   greater memory usage.  (Internally, it merges queues by inserting
   complete priority queues into other priority queues as members.)
   Unless you really need the optimal asymptotic performance, you'll
   probably be better off with one of the other options.

In general, the default (pairing heap) implementation is probably the best
choice.  The others have theoretically better worse-case performance, but
the default does better on average in most everyday scenarios.


Other Projects
--------------

Other Common Lisp priority queue libraries include:

 * [priority-queue][] - Uses a mutable in-place binary heap.

 * [damn-fast-priority-queue][] - Aims to do what it says: be fast.
   Mutable and only allows 32-bit fixnums as priority keys.

 * [cl-heap][] - Implements Fibonacci heap and binary heap classes and
   uses the Fibonacci heap to provide a priority queue.  Mutable.
   Unmaintained.

 * [queues][] - Provides a FIFO queue and priority queue, including
   thread-safe versions of each.  Mutable.  (Note that immutable data
   structures are inherently thread safe.)  The priority queue is
   implemented as an explicit binary heap.

 * [pileup][] - Thread-safe, performant, mutable priority queue.  Uses an
   implicit binary heap.

 * [minheap][] - Mutable implementations of several heap data structures.
   Key comparisons are hardwired: only provides min heaps over fixnum keys.

  [priority-queue]: https://github.com/dsorokin/priority-queue
  [damn-fast-priority-queue]: https://github.com/phoe/damn-fast-priority-queue
  [cl-heap]: https://github.com/TheRiver/CL-HEAP
  [queues]: https://github.com/oconnore/queues
  [pileup]: https://github.com/nikodemus/pileup
  [minheap]: https://github.com/sfrank/minheap


License
-------

To the extent possible under law, the author(s) have waived all copyright
and related or neighboring rights to this software.  A copy of the
Creative Commons Zero waiver should be included in this repository.  It is
also available at:

  https://creativecommons.org/publicdomain/zero/1.0/
