(in-package :functional-priority-queues)

;;;; Skew Binomial Queues


;;;;;;;;;;;;;;;;;;;;;
;;; Struct: sb-forest

(defstruct (sb-forest (:include functional-priority-queue))
  "A forest of skew binary queues."
  (queues nil
   :type list
   :read-only t))

(defun update-sb-forest (forest queues)
  (with-slots (key-fn pred-fn) forest
    (make-sb-forest :queues queues
                    :key-fn key-fn
                    :pred-fn pred-fn)))

(defun sb-forest-pprint (forest &optional (stream t))
  (dolist (queue (sb-forest-queues forest))
    (sb-queue-pprint queue stream)))


;;;;;;;;;;;;;;;;;;;
;;; Class: sb-queue

(defstruct (sb-queue (:constructor %make-sb-queue))
  (forest   nil :type sb-forest   :read-only t)
  (node     nil :type node        :read-only t)
  (rank       0 :type (integer 0) :read-only t)
  (children nil :type list        :read-only t))

(defun make-sb-queue (forest node)
  "Creates a rank 0 skew binary queue containing NODE and using FOREST's
  comparison functions."
  (%make-sb-queue :forest forest
                  :node node
                  :rank 0
                  :children nil))

(defun update-sb-queue (queue &key node rank (children nil childrenp))
  (with-slots (forest) queue
    (%make-sb-queue :node (or node (sb-queue-node queue))
                    :rank (or rank (sb-queue-rank queue))
                    :children (if childrenp children (sb-queue-children queue))
                    :forest forest)))

(defun sb-queue-pprint (queue &optional (stream t) (indent 0) (first t))
  (let ((queue-str (format nil "r~A: ~A~:[; ~A~;~]  "
                           (sb-queue-rank queue)
                           (node-priority (sb-queue-node queue))
                           (eql (node-priority (sb-queue-node queue))
                                (node-value (sb-queue-node queue)))
                           (node-value (sb-queue-node queue)))))
    (when (not first)
      (princ (make-string indent :initial-element #\ )
             stream))
    (princ queue-str stream)
    (if (zerop (sb-queue-rank queue))
        (princ #\Newline stream)
        (progn
          (sb-queue-pprint (first (sb-queue-children queue)) stream
                           (+ indent (length queue-str)) t)
          (dolist (child (cdr (sb-queue-children queue)))
            (sb-queue-pprint child stream
                             (+ indent (length queue-str)) nil))))))


;;;;;;;;;;;;;;;;;;;;;;
;;; API Method: empty?

(defmethod empty? ((forest sb-forest))
  (endp (sb-forest-queues forest)))


;;;;;;;;;;;;;;;;;;;;;;
;;; API Method: insert

(defun sb-link (queue1 queue2)
  (declare (type sb-queue queue1 queue2))
  (assert (= (sb-queue-rank queue1) (sb-queue-rank queue2)))
  (with-slots (forest) queue1
    (if (node-leq forest (sb-queue-node queue1) (sb-queue-node queue2))
        (update-sb-queue queue1
                         :rank (1+ (sb-queue-rank queue1))
                         :children (cons queue2 (sb-queue-children queue1)))
        (update-sb-queue queue2
                         :rank (1+ (sb-queue-rank queue2))
                         :children (cons queue1 (sb-queue-children queue2))))))

(defun sb-skew-link (queue0 queue1 queue2)
  (declare (type sb-queue queue0 queue1 queue2))
  (assert (= 0 (sb-queue-rank queue0)))
  (with-slots (forest) queue0
    (let ((n0 (sb-queue-node queue0))
          (n1 (sb-queue-node queue1))
          (n2 (sb-queue-node queue2)))
      (cond
        ((and (node-leq forest n1 n0)
              (node-leq forest n1 n2))
         (update-sb-queue queue1
                          :rank (1+ (sb-queue-rank queue1))
                          :children (cons queue0 (cons queue2 (sb-queue-children queue1)))))
        ((and (node-leq forest n2 n0)
              (node-leq forest n2 n1))
         (update-sb-queue queue2
                          :rank (1+ (sb-queue-rank queue2))
                          :children (cons queue0 (cons queue1 (sb-queue-children queue2)))))
        (t
         (update-sb-queue queue0
                          :rank (1+ (sb-queue-rank queue1))
                          :children (list queue1 queue2)))))))


(defun sb-forest-empty-insert-node (forest node)
  (update-sb-forest forest
                    (cons (make-sb-queue forest node)
                          (sb-forest-queues forest))))

(defun sb-forest-insert-node (forest node)
  (with-slots (queues) forest
    (destructuring-bind (q1 q2 &rest qs) queues
      (update-sb-forest forest
                        (if (= (sb-queue-rank q1) (sb-queue-rank q2))
                            (cons (sb-skew-link (make-sb-queue forest node)
                                                q1 q2)
                                  qs)
                            (cons (make-sb-queue forest node) queues))))))

(defun sb-forest-reinsert (forest node)
  (declare (type sb-forest forest)
           (type node node))
  (if (endp (rest (sb-forest-queues forest)))
      (sb-forest-empty-insert-node forest node)
      (sb-forest-insert-node forest node)))

(defmethod insert ((forest sb-forest) item &optional (priority nil priorityp))
  (let ((node (make-node forest item priority priorityp)))
    (sb-forest-reinsert forest node)))


;;;;;;;;;;;;;;;;;;;;
;;; API Method: meld

(defun sb-ins (queue queues)
  (if (endp queues)
      (list queue)
      (progn
        (assert (<= (sb-queue-rank queue) (sb-queue-rank (first queues))))
        (if (< (sb-queue-rank queue) (sb-queue-rank (first queues)))
            (cons queue queues)
            (sb-ins (sb-link queue (first queues))
                    (rest queues))))))

(defun sb-uniquify (queues)
  "Ensures that the first two elements of QUEUES have different ranks (by
  combining them if they have the same rank."
  (if (endp queues)
      queues
      (sb-ins (first queues) (rest queues))))

(defun sb-meld-unique (queues1 queues2)
  (cond
    ((endp queues1)
     queues2)
    ((endp queues2)
     queues1)
    ((< (sb-queue-rank (first queues1)) (sb-queue-rank (first queues2)))
     (cons (first queues1)
           (sb-meld-unique (rest queues1) queues2)))
    ((< (sb-queue-rank (first queues2)) (sb-queue-rank (first queues1)))
     (cons (first queues2)
           (sb-meld-unique queues1 (rest queues2))))
    (t  ; ranks are equal
     (sb-ins (sb-link (first queues1) (first queues2))
             (sb-meld-unique (rest queues1) (rest queues2))))))

(defmethod meld ((forest1 sb-forest) (forest2 sb-forest))
  (with-slots ((key-fn-1 key-fn) (pred-fn-1 pred-fn)) forest1
    (with-slots ((key-fn-2 key-fn) (pred-fn-2 pred-fn)) forest2
      (when (not (eq key-fn-1 key-fn-2))
        (error "Cannot meld queues; key functions differ: ~A, ~A"
               key-fn-1 key-fn-2))
      (when (not (eq pred-fn-1 pred-fn-2))
        (error "Cannot meld queues; predicate functions differ: ~A, ~A"
               pred-fn-1 pred-fn-2))))
  (update-sb-forest forest1
                    (sb-meld-unique (sb-uniquify (sb-forest-queues forest1))
                                    (sb-uniquify (sb-forest-queues forest2)))))


;;;;;;;;;;;;;;;;;;;;;;;;;
;;; API Method: find-root

(defmethod find-root ((forest sb-forest))
  (when (empty? forest)
    (error "Cannot get the root of an empty queue."))
  (with-slots (queues) forest
    (flet ((pick-node (node1 node2)
             (if (node-leq forest node1 node2)
                 node1
                 node2)))
      (let ((min-root (reduce #'pick-node queues
                              :key #'sb-queue-node)))
        (values (node-value min-root)
                (node-priority min-root))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; API Method: delete-root

(defmethod delete-root ((forest sb-forest))
  (when (empty? forest)
    (error "Cannot delete the root of an empty queue."))
  (labels ((extract-root (queues)
             (if (endp (cdr queues))
                 (values (car queues) nil)
                 (multiple-value-bind (cdr-root-queue cdr-other-queues)
                     (extract-root (cdr queues))
                   (if (node-leq forest
                                 (sb-queue-node (car queues))
                                 (sb-queue-node cdr-root-queue))
                       (values (car queues) (cdr queues))
                       (values cdr-root-queue (cons (car queues) cdr-other-queues))))))
           (extract-r0-nodes (queues non-r0-queues nodes)
             (cond
               ((endp queues)
                (values non-r0-queues nodes))
               ((zerop (sb-queue-rank (car queues)))
                (extract-r0-nodes (cdr queues)
                                  non-r0-queues
                                  (cons (sb-queue-node (car queues))
                                        nodes)))
               (t  ; Non-rank-zero queue
                (extract-r0-nodes (cdr queues)
                                  (cons (car queues) non-r0-queues)
                                  nodes)))))
    (multiple-value-bind (root-queue other-queues)
        (extract-root (sb-forest-queues forest))
      (multiple-value-bind (non-r0-root-queues root-nodes)
          (extract-r0-nodes (sb-queue-children root-queue) nil nil)
        (values (reduce (lambda (new-forest node)
                          (sb-forest-reinsert new-forest node))
                        root-nodes
                        :initial-value (meld (update-sb-forest forest non-r0-root-queues)
                                             (update-sb-forest forest other-queues)))
                (node-value (sb-queue-node root-queue))
                (node-priority (sb-queue-node root-queue)))))))
